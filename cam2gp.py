import csv
import datetime
from pprint import pprint as pp

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db, utils

ORIGIN_LBL = 'CAM Origin Data'
SURVEY_LBL = 'CAM Survey Data'

"""
Get two pivot tables for most recent month (includes all previous months):
http://www.mbie.govt.nz/info-services/sectors-industries/tourism/...
    ...tourism-research-data/commercial-accommodation-monitor/...
    ...cam-regional-pivot-tables

SURVEY - Regional Tourism Organisation (RTO) Area by Accommodation Type (pointing somewhere at stats.govt.nz like http://www.stats.govt.nz/~/media/Statistics/Browse%20for%20stats/AccommodationSurvey/HOTPJun17/...
    ...as-Jun17-pivot-table-RTO-and-accommodation-type.xlsx )
ORIGIN - Origin of Guests Information by Regional Tourism Organisation (RTO) (point somewhere at stats.govt.nz like http://www.stats.govt.nz/~/media/Statistics/Browse%20for%20stats/AccommodationSurvey/HOTPJun17/...
    ...as-Jun17-pivot-table-RTO-and-origin.xlsx )

In each spreadsheet, unhide the DPCache_Data sheet. The data in this sheet is
exactly in the form required and includes all data from 2000 onwards. Select
sheet and save as CSV (will save active sheet).


"""

sql_insert_origin_tpl = """\
    INSERT INTO {origin}
    (month, rto, domestic_guest_nights, international_guest_nights,
     total_guest_nights)
    VALUES (%s, %s, %s, %s, %s)
    """

sql_insert_survey_tpl = """\
    INSERT INTO {survey}
    (month, accom_type, rto, guest_nights,
     establishments, daily_capacity, monthly_capacity, occupancy_rate, guest_arrivals,
     stay_unit_nights)
    VALUES (
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s
    )
    """

def get_cam_data(fpath_origin, fpath_survey):
    debug = False
    verbose = False
    data = {}
    wipe_total_idxs_origin = [1, ]  ## rto only
    wipe_total_idxs_survey = [1, 2]  ## accom_type and rto
    fpaths = [
        (ORIGIN_LBL, fpath_origin, wipe_total_idxs_origin),
        (SURVEY_LBL, fpath_survey, wipe_total_idxs_survey), ]
    for lbl, fpath, wipe_total_idxs in fpaths:
        if lbl == SURVEY_LBL:
            print("")
        print("Processing '{}'".format(lbl))
        rows = []
        with open(fpath) as csvfile:
            reader = csv.reader(csvfile)
            for i, row in enumerate(reader):
                if i == 0:
                    continue
                skip_row = False
                for idx in wipe_total_idxs:
                    if row[idx].startswith("Total"):
                        skip_row = True
                        break
                if skip_row:
                    continue
                if debug:
                    if verbose:
                        pp(row)
                    else:
                        print(row)
                rows.append(row)
                if i % 1000 == 0:
                    print(utils.prog(i))
        data[lbl] = rows
    return data

def _make_test_cam_tbl(con_rem, cur_rem, lbl, tblname, sql_insert_tpl, rows):
    for n, row in enumerate(rows, 1):
        try:
            cur_rem.execute(sql_insert_tpl, row)
        except IndexError:
            raise
        if n % 100 == 0:
            print(utils.prog(n))
    con_rem.commit()
    sql_permissions = """\
    GRANT SELECT ON TABLE {tblname} TO public
    """.format(tblname=tblname)
    cur_rem.execute(sql_permissions)  ## Give public read permissions
    con_rem.commit()
    print("Finished making '{}'".format(lbl))

def _prep_rows(rows, idx_ints, idx_floats):
    cam_rows = []
    for row in rows:
        row = list(row)
        for idx in idx_ints:
            try:
                row[idx] = int(row[idx])
            except ValueError:
                row[idx] = None
        for idx in idx_floats:
            try:
                row[idx] = float(row[idx])
            except ValueError:
                row[idx] = None
        row[0] = (datetime.datetime.strptime(row[0], '%d/%m/%y')
            .strftime('%Y-%m'))
        cam_rows.append(row)
    return cam_rows

def make_test_cam_tbls(cam_data, remake_origin=True, remake_survey=True):
    con_rem, cur_rem = db.Pg.get_rem()
    if remake_origin:
        print("Remaking ORIGIN")
        db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.CAM_ORIGIN_TEST)
        sql_make_origin_tbl = """\
        CREATE TABLE {origin} (
            month text,
            rto text,
            domestic_guest_nights bigint,
            international_guest_nights bigint,
            total_guest_nights bigint
        )
        """.format(origin=conf.CAM_ORIGIN_TEST)
        cur_rem.execute(sql_make_origin_tbl)
        con_rem.commit()
        sql_insert_origin_test_tpl = sql_insert_origin_tpl.format(
            origin=conf.CAM_ORIGIN_TEST)
        origin_rows = _prep_rows(rows=cam_data[ORIGIN_LBL], idx_ints=[2, 3, 4],
            idx_floats=[])
        _make_test_cam_tbl(con_rem, cur_rem, ORIGIN_LBL, conf.CAM_ORIGIN_TEST,
            sql_insert_origin_test_tpl, origin_rows)
    if remake_survey:
        print("Remaking SURVEY")
        db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.CAM_SURVEY_TEST)
        sql_make_survey_tbl = """\
        CREATE TABLE {survey} (
            month text,
            accom_type text,
            rto text,
            guest_nights integer,
            establishments integer,
            daily_capacity integer,
            monthly_capacity integer,
            occupancy_rate double precision,
            guest_arrivals integer,
            stay_unit_nights integer
        )
        """.format(survey=conf.CAM_SURVEY_TEST)
        cur_rem.execute(sql_make_survey_tbl)
        con_rem.commit()
        sql_insert_survey_test_tpl = sql_insert_survey_tpl.format(
            survey=conf.CAM_SURVEY_TEST)
        survey_rows = _prep_rows(rows=cam_data[SURVEY_LBL],
            idx_ints=[3, 4, 5, 6, 8, 9], idx_floats=[7, ])
        _make_test_cam_tbl(con_rem, cur_rem, SURVEY_LBL, conf.CAM_SURVEY_TEST,
            sql_insert_survey_test_tpl, survey_rows)

def check_test_cam_tbls(new_month):
    """
    Check nothing unexpected in CAM data compared with previous values held.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql_check_origin_tpl = """\
    SELECT *
    FROM {test} AS test
    FULL OUTER JOIN
    {prod} AS prod
    ON test.month = prod.month
    AND test.rto = prod.rto
    -- look for any differences
    WHERE COALESCE(test.domestic_guest_nights, 666) != COALESCE(prod.domestic_guest_nights, 666)
    OR COALESCE(test.international_guest_nights, 666) != COALESCE(prod.international_guest_nights, 666)
    OR COALESCE(test.total_guest_nights, 666) != COALESCE(prod.total_guest_nights, 666)

    ORDER BY test.month, test.rto
    """.format(test=conf.CAM_ORIGIN_TEST, prod=conf.CAM_ORIGIN)
    cur_rem.execute(sql_check_origin_tpl)
    origin_data = cur_rem.fetchall()
    origin_sql = str(cur_rem.query, encoding='utf-8')
    sql_check_survey_tpl = """\
    SELECT *
    FROM {test} AS test
    FULL OUTER JOIN
    {prod} AS prod
    ON test.month = prod.month
    AND test.rto = prod.rto
    AND test.accom_type = prod.accom_type
    -- look for any differences
    WHERE COALESCE(test.guest_nights, 666) != COALESCE(prod.guest_nights, 666)
    OR COALESCE(test.establishments, 666) != COALESCE(prod.establishments, 666)
    OR COALESCE(test.daily_capacity, 666) != COALESCE(prod.daily_capacity, 666)
    OR COALESCE(test.monthly_capacity, 666) != COALESCE(prod.monthly_capacity, 666)
    OR COALESCE(test.occupancy_rate, 666) != COALESCE(prod.occupancy_rate, 666)
    OR COALESCE(test.guest_arrivals, 666) != COALESCE(prod.guest_arrivals, 666)
    OR COALESCE(test.stay_unit_nights, 666) != COALESCE(prod.stay_unit_nights, 666)

    ORDER BY test.month, test.rto
    """.format(test=conf.CAM_SURVEY_TEST, prod=conf.CAM_SURVEY)
    cur_rem.execute(sql_check_survey_tpl)
    survey_sql = str(cur_rem.query, encoding='utf-8')
    survey_data = cur_rem.fetchall()
    for lbl, data in [(ORIGIN_LBL, origin_data), (SURVEY_LBL, survey_data), ]:
        print("Processing '{}'".format(lbl))
        ok = True
        first_month = None
        for row in data:
            #print(row)
            month = row[0]
            if ok and month != new_month:
                first_month = month
                ok = False
        if ok:
            print("\nLooks good - all data is for newest month '{}'".format(
                new_month))
        else:
            utils.warn("Some data differs for months other than '{}'".format(
                new_month))
            if first_month:
                print("First month differing is '{}'".format(first_month))
    print("Confirm with:\n\n{origin_sql};\n\n{survey_sql};".format(
        origin_sql=origin_sql, survey_sql=survey_sql))

def update_prod_cam_tbls():
    """
    TRUNCATE otherwise wipe all the views on top of the table.
    """
    con_rem, cur_rem = db.Pg.get_rem()
    sql_trunc_cam_tbls = """\
    TRUNCATE {origin}, {survey}
    """.format(origin=conf.CAM_ORIGIN, survey=conf.CAM_SURVEY)
    cur_rem.execute(sql_trunc_cam_tbls)
    sql_insert_into_tpl = """\
    INSERT INTO {prod} SELECT * FROM {test}
    """
    select_into_dets = [
        (conf.CAM_ORIGIN, conf.CAM_ORIGIN_TEST),
        (conf.CAM_SURVEY, conf.CAM_SURVEY_TEST), ]
    for prod, test in select_into_dets:
        cur_rem.execute(sql_insert_into_tpl.format(prod=prod, test=test))
    con_rem.commit()  ## only commit if successfully repopulated
    print("FINISHED - Updated both CAM tables in production!")

def main():
    make_fresh_test_src = False

    remake_origin = True
    remake_survey = True

    make_fresh_prod = True
    if make_fresh_test_src:
        fname_origin = 'as-Jun17-pivot-table-RTO-and-origin.csv'
        fpath_origin = os.path.join(conf.CAM_ROOT, fname_origin)
        fname_survey = 'as-Jun17-pivot-table-RTO-and-accommodation-type.csv'
        fpath_survey = os.path.join(conf.CAM_ROOT, fname_survey)
        cam_data = get_cam_data(fpath_origin, fpath_survey)
        make_test_cam_tbls(cam_data, remake_origin, remake_survey)
    if make_fresh_prod:
        check_test_cam_tbls(new_month='2017-06')
        input("If data OK press any key to UPDATE PROD - otherwise abort")
        update_prod_cam_tbls()

if __name__ == '__main__':
    main()
